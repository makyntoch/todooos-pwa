import { html, render } from '../../web_modules/lit-html.js';
import '../../web_modules/lit-icon.js';

export default function Edit(page, data) {
  const props = {};
  function deleteTodo() {
    const event = new CustomEvent('delete-todo', { detail: data });
    document.dispatchEvent(event);
  }

  function updateTodo() {
    data.done = data.done === 'true'  ? 'false' : 'true';
    const event = new CustomEvent('update-todo', { detail: data });
    document.dispatchEvent(event);
  }

  const template = ({ data, props }) => html`
    <style>
      input:checked + svg {
        display: block;
      }
    </style> 
    <section class="rounder-lg bg-white mt-4 mx-4 px-4 py-4 shadow-sm">
      <header class="flex items-center">
        <label class="flex justify-start items-start" tabindex="0" aria-label="Check todo">
          <div class="bg-white border-2 rounded border-gray-400 w-6 h-6 flex flex-shrink-0 justify-center items-center focus:border-blue-500">
            <input type="checkbox" name="todo[]" class="opacity-0 absolute" tabindex="0"  ?checked="${data.done === 'true'}" @change=${updateTodo}>
            <svg class="fill-current hidden w-4 h-4 text-green-500 pointer-events-none" viewBox="0 0 20 20"><path d="M0 11l2-2 5 5L18 3l2 2L7 18z"/></svg>
          </div>
        </label>
        <h1 class="ml-2 text-2xl flex-1">${data.title}</h1>
        ${ data.synced === 'false' ? html`<lit-icon icon="cloud-off"></lit-icon>` : '' }
        <button @click="${deleteTodo}" class="ml-2 text-red-600" aria-label="Delete">
          <lit-icon icon="delete"></lit-icon>
        </button>
      </header>
      <main></main>
    </section>
  `;

  function renderView(data) {
    const view = template({ data, props });
    render(view, page);
  }

  renderView(data);

  document.addEventListener('render-view', ({ detail }) => {
    renderView(detail);
  });
}
