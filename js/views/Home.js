import { html, render } from '../../web_modules/lit-html.js';
import '../../web_modules/lit-icon.js';
import todoCard from '../components/todo-card.js';


export default function Home(page, data) {
  const props = {
    todo: '',
  };
  const template = ({ data, props }) => html`
    <section class="h-full">
      <div ?hidden="${!data.length}">
        <header>
          <h1 class="mt-2 px-4 text-xl">My Todooos : </h1>
        </header>
        <main class="todolist px-4 pb-20">
          <ul>
            ${data.map(todo => todoCard(todo))}
          </ul>
        </main>
      </div>
      <footer class="h-16 bg-gray-300 fixed bottom-0 inset-x-0">
        <form @submit="${handleForm}" id="addTodo" class="w-full h-full flex justify-between items-center px-4 py-3">
          <label class="flex-1" aria-label="Add todo input">
            <input
              autocomplete="off"
              .value="${props.todo}"
              @input="${e => props.todo = e.target.value}"
              class="py-3 px-4 rounded-sm w-full h-full"
              type="text"
              placeholder="New todo ..."
              name="todo">
          </label>
          <button
            aria-label="Add"
            class="ml-4 rounded-lg text-uppercase h-full text-center px-3 uppercase text-white font-bold flex justify-center items-center"
            type="submit">Add</button>
        </form>  
      </footer>
    </section>
  `;

  renderView(data);

  function handleForm(e) {
    e.preventDefault();
    if (props.todo === '') return console.warn('Value required !');

    const todo = {
      id: Date.now(),
      title: props.todo,
      synced: 'true',
      updated: 'false',
      done: 'false',
      deleted: 'false',
      date: Date.now()
    };

    const event = new CustomEvent('create-todo', { detail: todo });
    document.dispatchEvent(event);

    // Clean input
    props.todo = null;
    renderView(data);
  }

  function renderView(data) {
    const view = template({ data, props });
    render(view, page);
    const input = document.querySelector('[name="todo"]');
    input.value = '';
  }

  document.addEventListener('render-view', ({ detail }) => {
    renderView(detail);
  });
}
